#pragma once

#include <random>

enum NumberRange
{
    IDMIN = 0, IDMAX = 9
};

class RandNumGen
{
public:
    RandNumGen();

    void RandomSeedGenerate();
    void RandomNumRange(NumberRange min, NumberRange max);
    const int RandomNumGen() { return m_uniform(m_randomGen); }

    ~RandNumGen(); 

private:
    static bool seedGenerateState;

    std::random_device m_randomDevice;
    std::default_random_engine m_randomGen;
    std::uniform_int_distribution<int> m_uniform;
};

