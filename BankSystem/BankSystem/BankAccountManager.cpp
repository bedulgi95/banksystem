#include "BankAccountManager.h"

BankAccountManager::BankAccountManager()
    : m_accountID("")
{
}

BankAccountManager::BankAccountManager(std::string accountName, int firstDepositMoney)
    : m_accountID(""), m_accountName(accountName), m_accountMoney(firstDepositMoney)
{
    SetAccountRandomID();
}

BankAccountManager::BankAccountManager(BankAccountManager & copyAccount)
    : m_accountID("")
{
    this->SetAccountName(copyAccount.GetAccountName()).SetFirstDepositMoney(copyAccount.GetFirstDepositMoney());
}

inline BankAccountManager & BankAccountManager::SetAccountName(const std::string accountName)
{
    this->m_accountName = accountName;
    return *this;
}

BankAccountManager & BankAccountManager::SetAccountRandomID()
{
    RandomNumRange(IDMIN, IDMAX);

    do {
        m_accountID += std::to_string(RandomNumGen());
    } while (m_accountID.size() < IDSIZE);

    return *this;
}

inline BankAccountManager & BankAccountManager::SetFirstDepositMoney(const int firstDepositMoney)
{
    this->m_accountMoney = firstDepositMoney;
    return *this;
}

BankAccountManager::~BankAccountManager()
{
}