#pragma once

#include "RandomNumGen.h"

#include <string>

enum AccountNum
{
    IDSIZE = 9
};

class BankAccountManager : protected RandNumGen
{
public:
    BankAccountManager();
    BankAccountManager(std::string accountName, int firstDepositMoney);
    BankAccountManager(BankAccountManager & copyAccount);

    // 함수 내의 멤버변수의 값을 const(상수화)하여 변경하지 않음을 뜻함
    BankAccountManager & SetAccountName(const std::string accountName);
    BankAccountManager & SetAccountRandomID();
    BankAccountManager & SetFirstDepositMoney(const int firstDepositMoney);

    // 함수의 반환형을 const(상수화)하여 return 값을 변경하지 않음을 의미
    const std::string GetAccountName() { return m_accountName; }
    const std::string GetAccountID() { return m_accountID; }
    const int GetFirstDepositMoney() { return m_accountMoney; }

    ~BankAccountManager();

protected:
    std::string m_accountName;
    std::string m_accountID;
    int m_accountMoney;
};