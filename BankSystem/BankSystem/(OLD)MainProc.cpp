#define BankSystem_VERSION_MAJOR 1
#define BankSystem_VERSION_MINOR 0

#include "BankSystem.h"

#include <fstream>
#include <Windows.h>
#include <string>

void InsertFileToArray();
void InsertArrayToFile();

int InsertNumPos();

void Select1();
void Select2();
void Select3();
void Select4();

int SearchNameNum();

Account * BankPerson = new Account[100];
int personNum = InsertNumPos();

int main(int args, char * argv[])
{
	bool isUsing = true;

	while (isUsing)
	{
		if (isUsing == false) break;

		std::cout << "[Welcome To Bank " << BankSystem_VERSION_MAJOR << '.' << BankSystem_VERSION_MINOR << ']' << '\n' << '\n';
		Sleep(1000);

		std::cout << "\"Please select The Service\"" << '\n' << '\n';
		Sleep(1000);

		std::cout << "1. Account Create" << "(계좌개설)" << '\n';
		Sleep(500);
		std::cout << "2. Deposit The Money" << "(입금)" << '\n';
		Sleep(500);
		std::cout << "3. WhitDraw the Money" << "(출금)" << '\n';
		Sleep(500);
		std::cout << "4. All Acounts Reference" << "(모든 계좌 조회)" << '\n';
		Sleep(500);
		std::cout << "5. End The Bank System" << "(은행 시스템 종료)" << '\n';
		Sleep(500);

		std::cout << '\n' << "Select : ";

		int selNum;

		std::cin >> selNum;

		std::cout << '\n' << "잠시만 기다려주세요";

		for (int progress = 0; progress < 8; progress++)
		{
			std::cout << '.' << ' ';
			Sleep(200);
		}

		std::cout << '\n';

        // 최초 실행시 기록할 파일이 존재하지 않으므로 personNum이 증가하지 않음
		InsertFileToArray();

		// DEBUG
		// system("pause");

		switch (selNum)
		{
		case 1:
			system("cls");
			Select1();
			system("cls");
			personNum++;
			break;
		case 2:
			system("cls");
			Select2();
			system("cls");
			break;
		case 3:
			system("cls");
			Select3();
			system("cls");
			break;
		case 4:
			system("cls");
			Select4();
			system("cls");
			break;
		case 5:
			std::cout << '\n' << "정상적으로 처리가 완료되었습니다." << '\n' << "프로그램을 종료합니다" << '\n';
			isUsing = false;
			Sleep(1000);
			break;
		default:
			std::cout << '\n' << "다시 입력해주세요.";
			Sleep(2000);
			system("cls");
			break;
		}

		InsertArrayToFile();
	}

	std::cout << '\n' << "[Bank " << BankSystem_VERSION_MAJOR << '.' << BankSystem_VERSION_MINOR << " System] ";
	std::cout << "이용해주셔서 감사합니다." << '\n';

	

	system("pause");
	
	// delete[] BankPerson;
	
	return 0;
}

void InsertFileToArray()
{
    char * name = new char[20];
    int num;
    int money;

    std::ifstream insertFromFile;

    insertFromFile.open("BankPerson.txt", std::ios::in);

    // 파일("BankPerson.txt")의 존재 여부 확인
    // BankPerson.txt이 존재하면 insertFromFile이 true값을 반환
    if (insertFromFile)
    {
        for (int i = 0; insertFromFile >> name >> num >> money; i++)
        {
            BankPerson[i].SetMoney(money).SetAccountNum(num).SetName(name).SetIsFilled(true);
            personNum = i;
        }

        personNum++;
    }

	/* DEBUG
	for (int num = 0; num < personNum; num++)
	{
		std::cout << BankPerson[num].GetPersonName() << '\n';
		std::cout << BankPerson[num].GetPersonMoney() << '\n';
		std::cout << BankPerson[num].GetPersonAccountNum() << '\n';
	}
	*/

	insertFromFile.close();

	delete[] name;
}

void InsertArrayToFile()
{
	std::ofstream outputToFile("BankPerson.txt", std::ios::out);

	for (int num = 0; num < personNum; num++)
	{
		outputToFile << BankPerson[num].GetPersonName() << '\n';
		outputToFile << BankPerson[num].GetPersonAccountNum() << '\n';
		outputToFile << BankPerson[num].GetPersonMoney() << '\n';
	}

	outputToFile.close();
}

int InsertNumPos()
{
	for (int num = 0; num < 100; num++)
	{
		if (num == 0 && BankPerson[num].GetIsFilled() == false) {
			return 0;
		}
		else if (!BankPerson[num].GetIsFilled()) {
			return num++;
		}
	}
}

void Select1()
{
	// std::cout << "[DEBUG] personNum : " << personNum << '\n';
	std::cout << "계좌 개설을 시작합니다." << '\n' << '\n';

	char * name = new char[20];
	int baseMoney;

	std::cout << "고객님 성함 : ";
	std::cin >> name;

	do
	{
		std::cout << "신규개설 입금금액 (반드시 1원 이상이어야 합니다) : ";
		std::cin >> baseMoney;

		if (baseMoney < 0) {
			std::cout << '\n' << "입금금액이 잘못되었습니다. 다시 입력하여주세요.";
			Sleep(2000);
			system("cls");
			std::cout << "계좌 개설을 시작합니다." << '\n' << '\n';
			std::cout << "고객님 성함 : " << name << '\n';
		}
	} while (baseMoney < 0);

	BankPerson[personNum].CreateAccount(name, baseMoney).RandAccountNumGen().SetIsFilled(true);

	/* [DEBUG]
	std::cout << "[DEBUG] Account Name : " << BankPerson[personNum].GetPersonName() << '\n';
	std::cout << "[DEBUG] Account Num : " << BankPerson[personNum].GetPersonAccountNum() << '\n';
	std::cout << "[DEBUG] Account Money : " << BankPerson[personNum].GetPersonMoney() << '\n';
	*/
	
	std::cout << '\n';

	for (int loading = 0; loading < 3; loading++)
	{
		std::cout << "계좌 개설중입니다.." << 3 - loading << '\n';
		Sleep(1000);
	}

	delete[] name;

	Sleep(500);
	std::cout << '\n' << "계좌 개설이 완료되어 이전 화면으로 돌아갑니다." << '\n' << '\n';
	Sleep(500);

	for (int loading = 0; loading < 3; loading++)
	{
		std::cout << "마치는 중.." << 3 - loading << '\n';
		Sleep(1000);
	}
}

void Select2()
{
	int inputMoneyNum = SearchNameNum();
	int money;
	std::cout << "현재 " << BankPerson[inputMoneyNum].GetPersonName() << "고객님의 잔액은 \'" << BankPerson[inputMoneyNum].GetPersonMoney() << "원\' 입니다." << '\n' << '\n';
	
	std::cout << "입금" << "하고자 하는 금액을 입력하세요" << '\n';
	std::cout << "→ ";
	std::cin >> money;

	std::cout << '\n';

	if (money < 1) { return; }

	BankPerson[inputMoneyNum].ProcessMoney(money, DEPOSIT);

	for (int loading = 0; loading < 3; loading++)
	{
		std::cout << "입금" << "중.." << 3 - loading << '\n';
		Sleep(1000);
	}

	std::cout << '\n';

	std::cout << "입금이 완료되었습니다. 현재 " << BankPerson[inputMoneyNum].GetPersonName() << "고객님의 잔액은 \'" << BankPerson[inputMoneyNum].GetPersonMoney() << "원\' 입니다." << '\n';

	for (int loading = 0; loading < 3; loading++)
	{
		std::cout << "마치는 중.." << 3 - loading << '\n';
		Sleep(1000);
	}
}

void Select3()
{
	int inputMoneyNum = SearchNameNum();
	int money;
	std::cout << "현재 " << BankPerson[inputMoneyNum].GetPersonName() << "고객님의 잔액은 \'" << BankPerson[inputMoneyNum].GetPersonMoney() << "원\' 입니다." << '\n' << '\n';

	std::cout << "출금" << "하고자 하는 금액을 입력하세요" << '\n';
	std::cout << "→ ";
	std::cin >> money;

	std::cout << '\n';

	if (money < 1) { return; }

	BankPerson[inputMoneyNum].ProcessMoney(money, WITHDRAW);

	for (int loading = 0; loading < 3; loading++)
	{
		std::cout << "출금" << "중.." << 3 - loading << '\n';
		Sleep(1000);
	}

	std::cout << '\n';

	std::cout << "출금" << "이 완료되었습니다. 현재 " << BankPerson[inputMoneyNum].GetPersonName() << "고객님의 잔액은 \'" << BankPerson[inputMoneyNum].GetPersonMoney() << "원\' 입니다." << '\n';

	for (int loading = 0; loading < 3; loading++)
	{
		std::cout << "마치는 중.." << 3 - loading << '\n';
		Sleep(1000);
	}
}

void Select4()
{
	for (int num = 0; num < personNum; num++)
	{
		std::cout << "<< " << num + 1 << "번 고객" << " >>" << '\n';
		std::cout << "이름 : " << BankPerson[num].GetPersonName() << '\n';
		std::cout << "계좌번호 : " << BankPerson[num].GetPersonAccountNum() << '\n';
		std::cout << "계좌잔액 : " << BankPerson[num].GetPersonMoney() << '\n' << '\n';
	}

	std::cout << "아무 키를 입력하시면 메인화면으로 돌아갑니다." << '\n';

	system("pause");
}

int SearchNameNum()
{
	std::string name;
	std::string arrayName;

	std::cout << "고객님의 이름을 입력하세요." << '\n';
	std::cout << "→ ";

	std::cin >> name;

	for (int num = 0; num < 100; num++)
	{
		arrayName = BankPerson[num].GetPersonName();
		if (name == arrayName) {
			return num;
		}
	}

	return 0;
}
