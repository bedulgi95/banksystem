#include "RandomNumGen.h"

RandNumGen::RandNumGen()
{
}

void RandNumGen::RandomSeedGenerate()
{
    m_randomGen.seed(m_randomDevice());
    seedGenerateState = true;
}

void RandNumGen::RandomNumRange(NumberRange min, NumberRange max)
{
    if (seedGenerateState == false) RandomSeedGenerate();

    decltype(m_uniform.param()) range(min, max);
    m_uniform.param(range);
}

RandNumGen::~RandNumGen()
{
}
